module Main (main) where

import System.Process
import Text.Printf

-- TODO: add frequency-dependent volume reduction (ie use gainDelta)

playFileFromStereo :: FilePath -> Float  -> Float  -> Float  -> Float -> IO ProcessHandle
playFileFromStereo fp c00 c01 c10 c11 = runCommand 
            $ "ffplay -autoexit -nodisp -v 0 -af \"" ++ af ++ "\" " ++ fp
            where 
                af = printf "pan='stereo| c0< %f * c0 + %f * c1 | c1< %f * c0 + %f * c1'" c00 c01 c10 c11

playFile :: FilePath -> Float  -> Float -> IO ProcessHandle
playFile fp dl dr = runCommand 
            $ "ffplay -autoexit -nodisp -v 0 -af \"" ++ af ++ "\" " ++ fp
            where 
                af = printf "pan='stereo|c0<FC|c1<FC', adelay='%f|%f'" dl dr
                        

playFileAngleFromStereo :: FilePath -> Float -> IO ProcessHandle
playFileAngleFromStereo fp θ = playFileFromStereo fp (cos θ) (sin θ) (-sin θ) (cos θ)

playFileAngle :: FilePath -> Float -> IO ProcessHandle
playFileAngle fp θ = playFile fp (max 0 $ timeDelayEars θ) (max 0 $ negate $ timeDelayEars θ)

-- formula: Δt := 1.234ms * sin θ  
-- (assuming sound is travelling in the air at sea level ~20°C; head is 14cm in diameter)
timeDelayEars :: Float -> Float
timeDelayEars = (*1.234) . sin

-- formula: ΔG := 1 + (f/1kHz)^.8 * sin θ
gainDelta :: Float -> Float -> Float
gainDelta f θ = 1 + (f/1000)**0.8 * sin θ

main :: IO ()
main = do 
        flip traverse [0..24] $ \i -> do
            --putStrLn $ printf "playing @    θ = %.0fπ/12" i
            putStrLn $ printf "playing @    theta = %.0fpi/12" i
            ph <- playFileAngle "src/fasthammer-mono.ogg" $ i * 2 * pi / 24
            waitForProcess ph
            return ()
        putStrLn "done playing."
        return ()